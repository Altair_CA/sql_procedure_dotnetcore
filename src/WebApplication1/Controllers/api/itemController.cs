﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models;
using WebApplication1.Data;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Hosting;
using System.Dynamic;
// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApplication1.Controllers.api
{
    [Route("api/[controller]")]
    public class itemController : Controller
    {
        private readonly ApplicationDbContext _db;
        private readonly IHostingEnvironment _env;
        public itemController(ApplicationDbContext db, IHostingEnvironment env)
        {
            _db = db;
            _env = env;
        }

        [HttpGet]
        public IActionResult get()
        {
            IEnumerable<dynamic> data = GetData("exec getItems");

            List<ItemsDTO> list = new List<ItemsDTO>();

            foreach(dynamic temp in data)
            {
                list.Add(new ItemsDTO
                {
                    id = (int)temp.id,
                    name = (string)temp.name
                });
            }
            return Json(list);
            
        }

        public IEnumerable<dynamic> GetData(String cmdText)
        {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory());
            builder.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{_env.EnvironmentName}.json", optional: true);
            var connectionStringConfig = builder.Build();
            using (var connection = new SqlConnection(connectionStringConfig.GetConnectionString("DefaultConnection")))
            {
                connection.Open();

                using (var command = new SqlCommand(cmdText, connection))
                {
                    using (var dataReader = command.ExecuteReader())
                    {
                        var fields = new List<String>();

                        for (var i = 0; i < dataReader.FieldCount; i++)
                        {
                            fields.Add(dataReader.GetName(i));
                        }

                        while (dataReader.Read())
                        {
                            var item = new ExpandoObject() as IDictionary<String, Object>;

                            for (var i = 0; i < fields.Count; i++)
                            {
                                item.Add(fields[i], dataReader[fields[i]]);
                            }

                            yield return item;
                        }
                    }
                }
            }
        }
    }
}
